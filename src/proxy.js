const httpProxy = require("http-proxy");

class Proxy {
  #config;
  #proxy;

  constructor(config) {
    this.#config = config;
    this.#proxy = httpProxy.createProxyServer({
      target: {
        host: config.sftp.host,
        port: config.proxy.targetPort,
      },
    });
  }

  start() {
    this.#proxy.listen(this.#config.proxy.localPort);
  }

  close() {
    this.#proxy.close();
  }
}

module.exports = Proxy;
