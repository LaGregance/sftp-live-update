#!/usr/bin/env node
const path = require("path");
const fs = require("fs");

const Watcher = require("./watcher");
const Proxy = require("./proxy");
const SSH = require("./ssh");

const getConfig = (configFile) => {
  const config = JSON.parse(fs.readFileSync(configFile, { encoding: "utf8" }));
  if (config.sftp.privateKey) {
    if (fs.existsSync(config.sftp.privateKey)) {
      config.sftp.privateKey = fs.readFileSync(config.sftp.privateKey);
    }
  }
  config.watchDir = path.resolve(path.normalize(config.watchDir));
  config.ignore =
    config.ignore && config.ignore.map((p) => path.join(config.watchDir, p));
  return config;
};

const main = async () => {
  let configFile = ".sftp-live-update.json";
  if (process.argv.length > 2) configFile = process.argv[2];

  if (!fs.existsSync(configFile)) {
    console.info("Usage: sftp-live-update [config]");
    console.info("default config: `.sftp-live-update.json`");
    return;
  }

  const config = getConfig(configFile);

  const watcher = new Watcher(config);
  await watcher.start();

  if (config.proxy && config.proxy.localPort && config.proxy.targetPort) {
    const proxy = new Proxy(config);
    proxy.start();
  }

  if (config.commands && Array.isArray(config.commands)) {
    const ssh = new SSH(config);
    ssh.start();
  }
};
main();
