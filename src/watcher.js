const chokidar = require("chokidar");
const path = require("path");
const fs = require("fs");
const anymatch = require("anymatch");
const SFTPClient = require("ssh2-sftp-client");
const Queue = require("./queue");
const colors = require("./colors");

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

class Watcher {
  #config;
  #sftp;
  #watcher;
  #queue;
  #fileToWait = [];
  #logPrefix = `${colors.yellow}[WATCHER] ${colors.reset}`;

  constructor(config) {
    this.#config = config;
    this.#sftp = new SFTPClient();
    this.#queue = new Queue();
    this.listFileToWait(config.watchDir);
  }

  listFileToWait(dir) {
    const files = fs.readdirSync(dir);
    for (const file of files) {
      const fullPath = path.join(dir, file);
      if (!this.#config.ignore || !anymatch(this.#config.ignore, fullPath)) {
        if (fs.lstatSync(fullPath).isDirectory()) {
          this.listFileToWait(fullPath);
        } else {
          this.#fileToWait.push(fullPath);
        }
      }
    }
  }

  async start() {
    await this.#sftp.connect(this.#config.sftp);
    this.#watcher = chokidar.watch(this.#config.watchDir, {
      awaitWriteFinish: true,
      persistent: true,
      ignored: this.#config.ignore,
    });

    this.#watcher.on("addDir", this.handleAddDir);
    this.#watcher.on("add", this.handleAddFile("ADD FILE", colors.green));
    this.#watcher.on(
      "change",
      this.handleAddFile("UPDATE FILE", colors.yellow)
    );
    this.#watcher.on("unlink", this.handleFileDelete);
    this.#watcher.on("unlinkDir", this.handleDirDelete);

    while (this.#fileToWait.length > 0) {
      await sleep(500);
    }
    console.log(this.#logPrefix + "✅ Initial upload");
  }

  handleAddDir = (path) => {
    this.#queue.addToQueue(async () => {
      const target = path.replace(
        this.#config.watchDir,
        this.#config.targetDir
      );
      console.log(
        this.#logPrefix +
          colors.green +
          "ADD DIRECTORY " +
          colors.reset +
          target
      );
      await this.#sftp.mkdir(target, true);
    });
  };

  handleAddFile = (eventName, color) => {
    return (path) => {
      this.#queue.addToQueue(async () => {
        const target = path.replace(
          this.#config.watchDir,
          this.#config.targetDir
        );
        const dir = target.substr(0, target.lastIndexOf("/"));
        console.log(
          this.#logPrefix + color + eventName + colors.reset + " " + target
        );
        await this.#sftp.mkdir(dir, true);
        await this.#sftp.put(path, target, {
          flags: "w",
          encoding: null,
          mode: 0o666,
          autoClose: true,
        });

        if (this.#fileToWait.includes(path)) {
          this.#fileToWait.splice(this.#fileToWait.indexOf(path), 1);
        }
      });
    };
  };

  handleFileDelete = (path) => {
    this.#queue.addToQueue(async () => {
      const target = path.replace(
        this.#config.watchDir,
        this.#config.targetDir
      );
      console.log(
        this.#logPrefix + colors.red + "DELETE FILE " + colors.reset + target
      );
      await this.#sftp.delete(target);
    });
  };

  handleDirDelete = (path) => {
    queue.addToQueue(async () => {
      const target = path.replace(
        this.#config.watchDir,
        this.#config.targetDir
      );
      console.log(
        this.#logPrefix +
          colors.green +
          "DELETE DIRECTORY " +
          colors.reset +
          target
      );
      await this.#sftp.rmdir(target);
    });
  };
}

module.exports = Watcher;
