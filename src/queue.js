class Queue {
  #callStack = [];
  #task;

  addToQueue(fct) {
    this.#callStack.push(fct);
    clearTimeout(this.#task);
    this.#task = setTimeout(this.flushQueue.bind(this), 20);
  }

  async flushQueue() {
    let it;
    while ((it = this.#callStack.shift())) {
      try {
        await it();
      } catch (e) {
        console.error(e);
      }
    }
  }
}

module.exports = Queue;
