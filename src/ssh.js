const colors = require("./colors");
const { Client } = require("ssh2");

class SSH {
  #config;
  #conn;
  #dataReceived = false;
  #logPrefix = `${colors.green}[SSH] ${colors.reset}`;

  constructor(config) {
    this.#config = config;
    this.#conn = new Client();
  }

  start() {
    this.#conn.on("ready", this.handleReady);
    this.#conn.connect(this.#config.sftp);
  }

  handleReady = () => {
    this.#conn.shell((err, stream) => {
      if (err) throw err;

      stream.on("close", () => {
        console.log(this.#logPrefix + "Connection closed");
        this.#conn.end();
      });

      stream.on("data", (data) => {
        if (!this.#dataReceived) {
          process.stdout.write(this.#logPrefix);
          this.#dataReceived = true;
        }
        const strData = `${data}`.replace("\n", "\n" + this.#logPrefix);
        process.stdout.write(strData);
      });

      stream.write(`cd "${this.#config.targetDir}"\n`);
      this.#config.commands.forEach((cmd) => {
        stream.write(`${cmd}\n`);
      });
      stream.end();
    });
  };
}

module.exports = SSH;
