# sftp-live-update

This tool allows you to work on a local copy, listen to change in order to instantly send them to a remote
SFTP server. Also you can run a command via SSH on the server & configure proxy.

## Use case & Genesis

When I work hard on a project with database inside docker, backend and multiple front end,
my laptop said something like "I don't get paid enough for this bullshit" and it starts to grumble.
Lucky as I am, I have an unused linux server at my home. With `sftp-live-update` I will show you
how I use this server to run my backend project while I'm still working on my local copy.
It allows me to save my precious memory on my computer.

## Feature

- 🚀 Automatically send update on an SFTP server
- 👻 Setup a proxy from localhost to your server
- ✈️ Run a command directly after upload by SSH

## Requirements

- Obviously an SFTP / SSH server
- A folder to watch : in my case a nodejs project

## Installation

```
yarn global add sftp-live-update
```

## Usage

Create a config file (json):

```js
{
  "sftp": {
    "host": "your sftp host",
    "port": 22,
    "username": "your sftp user",

    "privateKey": "/Users/gtaja/.ssh/id_rsa", // You can put a file or the content of your key
    "passphrase": "your passphrase",
    // or
    "password": "your password",

    // You can also pass any parameters accepted by the connect function
    // of https://github.com/theophilusx/ssh2-sftp-client
  },
  "watchDir": "directory to watch in your laptop",
  "targetDir": "directory to target in your Watcher server",
  "ignore": [
    // Ignore theses files : anymatch-compatible definition
    // https://github.com/es128/anymatch
    "/node_modules",
    "/dist",
    "/.git",
    "/.idea"
  ],
  // Optional
  "proxy": {
    // Redirect HTTP Connection from http://{sftp.host}:${proxy.targetPort}
    "targetPort": 3000,
    // To http://localhost:${proxy.localPort}
    "localPort": 3000
  },
  // Optional
  // Run interactive shell on target server in the targetDir
  "commands": [
    // Install your project
    "yarn install",
    // Start your project
    "yarn start:dev"
  ]
}
```

Run `sftp-live-update <config>` (make sure you have add yarn binary to PATH `export PATH="$(yarn global bin):$PATH"`)  
**WARNING:** This will update every files in your Watcher server under the targetDir, make sure this dir doesn't
contains important things.

Tricks: running `sftp-live-update` without config file will look in the file `.sftp-live-update.json`

For my use case, with the configured proxy & commands, I just run `sftp-live-update` and my app is
running on my server with the same confort as on my device (it's so good to use `localhost` to target the
remote server) !

Enjoy ;)
